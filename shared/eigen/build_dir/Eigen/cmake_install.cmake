# Install script for directory: /home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE FILE FILES
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/Cholesky"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/CholmodSupport"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/Core"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/Dense"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/Eigen"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/Eigenvalues"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/Geometry"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/Householder"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/IterativeLinearSolvers"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/Jacobi"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/KLUSupport"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/LU"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/MetisSupport"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/OrderingMethods"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/PaStiXSupport"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/PardisoSupport"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/QR"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/QtAlignedMalloc"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/SPQRSupport"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/SVD"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/Sparse"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/SparseCholesky"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/SparseCore"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/SparseLU"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/SparseQR"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/StdDeque"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/StdList"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/StdVector"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/SuperLUSupport"
    "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/UmfPackSupport"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE DIRECTORY FILES "/home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/Eigen/src" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

