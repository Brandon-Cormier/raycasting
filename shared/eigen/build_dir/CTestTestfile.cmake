# CMake generated Testfile for 
# Source directory: /home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen
# Build directory: /home/brandon/Documents/CSC418/a2/computer-graphics-ray-casting/shared/eigen/build_dir
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("Eigen")
subdirs("doc")
subdirs("test")
subdirs("blas")
subdirs("lapack")
subdirs("unsupported")
subdirs("demos")
subdirs("scripts")
subdirs("bench/spbench")
